package com.tulingxueyuan.order.controller;

import com.tulingxueyuan.order.pojo.Order;
import com.tulingxueyuan.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * @author 飞
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;

    // 插入订单信息
    @RequestMapping("/add")
    public String add(){
        Order order=new Order();
        order.setProductId(9);
        order.setStatus(0);
        order.setTotalAmount(100);
        orderService.create(order);
        return "下单成功";
    }

    @RequestMapping("/find")
    public String find(){
        Order order=new Order();
        order.setProductId(9);
        order.setStatus(0);
        order.setTotalAmount(100);
        return  orderService.find(order);
    }

    @RequestMapping("/all")
    public List<Order> getAll() throws InterruptedException {
        TimeUnit.SECONDS.sleep(6);
        return orderService.all();
    }

    // 获取单个订单信息
    @RequestMapping("/get/{id}")
    public Order get(@PathVariable Integer id){
        return orderService.get(id);
    }
}

package com.wang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.controller
 * @Author: wang fei
 * @CreateTime: 2023-01-16  16:54
 * @Description: TODO
 * @Version: 1.0
 */

@SpringBootApplication
public class StockApplication {
    public static void main(String[] args) {
        SpringApplication.run(StockApplication.class, args);
    }
}

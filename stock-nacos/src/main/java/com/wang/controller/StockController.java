package com.wang.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.controller
 * @Author: wang fei
 * @CreateTime: 2023-01-16  16:51
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/stock")
public class StockController {
    @Value("${server.port}")
    String port;

    @RequestMapping("/reduct")
    public String reduct(@RequestHeader("token") String token) throws InterruptedException {
        System.out.println("扣减库存");
        return "扣减库存:"+port+" "+token;
    }


    @RequestMapping("/reduct2")
    public String reduct2() {
        System.out.println("扣减库存");
        return "扣减库存:"+port;
    }
}


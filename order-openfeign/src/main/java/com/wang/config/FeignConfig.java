package com.wang.config;

import com.wang.intercptor.CustomFeignInterceptor;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.config
 * @Author: wang fei
 * @CreateTime: 2023-01-20  15:26
 * @Description: TODO
 * @Version: 1.0
 */


/**
 * 配置Feign的日志
 * 注意： 此处配置@Configuration注解就会全局生效，如果想指定对应微服务生效，就不能配置
 *  全局配置： 当使用@Configuration 会将配置作用所有的服务提供方
 *  局部配置： 1. 通过配置类：如果只想针对某一个服务进行配置， 就不要加@Configuration
 *           2. 通过配置文件
 * @author 飞
*/
//@Configuration
public class FeignConfig {
    @Bean
    public Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }

    @Bean
    public CustomFeignInterceptor feignAuthRequestInterceptor(){
        return new CustomFeignInterceptor();
    }
}

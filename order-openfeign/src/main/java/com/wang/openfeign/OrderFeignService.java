package com.wang.openfeign;

import com.wang.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author wang fei
 * @description:
 * 开启Feign, 根据不同的url，传入不同的参
 * name 指定调用rest接口所在的服务名称
 * path 指定调用rest接口所在的Controller指定的@RequestMapping映射路径
 * 局部配置，让调用的微服务生效，在@FeignClient注解中指定使用的配置类,configuration = FeignConfig.class
**/
@FeignClient(name = "stock-nacos",path = "/stock",configuration = FeignConfig.class)
public interface OrderFeignService {
    //声明需要调用rest接口对应的方法
    @RequestMapping("/reduct")
     String reduct();

    @RequestMapping("/reduct2")
     String reduct2();
}

package com.wang.controller;

import com.wang.openfeign.OrderFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang.controllter
 * @Author: wang fei
 * @CreateTime: 2023-01-16  16:48
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    OrderFeignService orderFeignService;



    @GetMapping("/pay")
    public String pay(){
        String msg = orderFeignService.reduct();
        System.out.println("小王支付成功" + msg);
        return msg;
    }
    @GetMapping("/reduct2")
    public String reduct2(){
        String msg = orderFeignService.reduct();
        System.out.println("小王支付成功" + msg);
        return msg;
    }
}

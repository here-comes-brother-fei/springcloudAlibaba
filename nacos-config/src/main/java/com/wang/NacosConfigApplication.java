package com.wang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @BelongsProject: SpringCloudAlibabaLearn
 * @BelongsPackage: com.wang
 * @Author: wang fei
 * @CreateTime: 2023-01-20  16:45
 * @Description: TODO
 * @Version: 1.0
 */
@SpringBootApplication
public class NacosConfigApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(NacosConfigApplication.class, args);
        String userName = applicationContext.getEnvironment().getProperty("user.name");
        String userAge = applicationContext.getEnvironment().getProperty("user.age");
        System.err.println("username :"+userName+"; age: "+userAge);
    }
}
